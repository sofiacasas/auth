'use strict'


//Salt1:  $2b$10$bTOU/.gEH3fjCaYqbAuv.O
//Hash1:  $2b$10$bTOU/.gEH3fjCaYqbAuv.OujQlKlg1YwNmb7XI2jVA5uMR7dJXZgm

//Importamos módulos de criptografía
const bcrypt=require ('bcrypt');
//Simulamos datos
const miPass = 'miContraseña';
const basPass = 'otropassword';

//Creamos un salto
bcrypt.genSalt(10,(err,salt)=>{
    console.log(`Salt1:  ${salt}`);
    //Utilizamos el salt para generar un hush para el password
    bcrypt.hash(miPass,salt,(err,hash)=>{
        if(err) console.log(err);
        else console.log(`Hash1:  ${hash}`);
    });
});
