'use strict'

const passService  = require('./services/pass.service');
const moment = require ('moment');


//Datos simulados
const miPass="1234";
const badPass="6789";
const usuario = {
    _id :"123456789654332",
    email : "scl47@gcloud.ua.es",
    displayName : "sofia",
    password : miPass,
    signupDate: moment().unix(),
    lastlogin: moment().unix()

};
console.log(usuario);

//Encriptamos el pass
passService.encriptaPassword(usuario.password)
    .then ( hash => {
        usuario.password = hash;
        console.log(usuario);

        //verificamos el pass
        passService.compararPassword(miPass, usuario.password)
            .then (isOk => {
                if (isOk){
                    console.log("p1: El pass es correcto");
                }else{
                    console.log("p1: El pass es incorrecto");
                }

            });
            //verificamos el pass
            passService.compararPassword(badPass, usuario.password)
                .then (isOk => {
                    if (isOk){
                        console.log("p2: El pass es correcto");
                    }else{
                        console.log("p2: El pass es incorrecto");
                    }

                });
    });

 
