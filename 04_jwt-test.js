'use strict'

const tokenService= require('./services/token.service');
const moment = require ('moment');

//Datos simulados
const miPass="1234";
const badPass="6789";
const usuario = {
    _id :"123456789654332",
    email : "scl47@gcloud.ua.es",
    displayName : "sofia",
    password : miPass,
    signupDate: moment().unix(),
    lastlogin: moment().unix()

};
console.log(usuario);
//Creeamos un token
const tokens=tokenService.crearToken(usuario);
console.log(tokens);
//Decodificamos el token
tokenService.decodificarToken(tokens)
    .then(userID => {
        return console.log(`ID: ${userID}`);

    }, err=> {return console.log({Error1: err})})
    .catch(err => {
        return console.log({Error1: err});
    });